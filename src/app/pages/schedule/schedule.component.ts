import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { ClassementService } from 'src/app/service/basketBall/classement.service';
import { CompetitionService } from 'src/app/service/basketBall/competition.service';
import { GameService } from 'src/app/service/basketBall/game.service';
import { PhaseService } from 'src/app/service/basketBall/phase.service';
import { WeekService } from 'src/app/service/basketBall/week.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css'],
  encapsulation : ViewEncapsulation.None,
})
export class ScheduleComponent implements OnInit {
  games:any = [];
  phase:any = [];
  classement:any=[];
  comptetition:any= [];
  week:any=[];
  logoTeamHome:string="";
  phase_id_selected:any;
  logoTeamAway:string="";
  ngOnInit(): void {
    this.getAllCompetition();
    this.initClassementByPhaseId();
    //this.getAllPhase();
    //this.getAllWeeks();
   // this.getAllGames();
  }
  constructor(private phaseService:PhaseService,
              private competitionService:CompetitionService,
              private weekServices:WeekService,
              private gameServices:GameService,
              private classementService:ClassementService){
  }
  
  arrayBufferToBase64(buffer: ArrayBuffer): string {
    const binary = new Uint8Array(buffer);
    const bytes = Array.from(binary);
    return btoa(bytes.map((byte) => String.fromCharCode(byte)).join(''));
  }
  base64ToArrayBuffer(base64: string): ArrayBuffer {
    const binaryString = atob(base64);
    const bytes = new Uint8Array(binaryString.length);
    for (let i = 0; i < binaryString.length; i++) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes.buffer;
}
  getAllCompetition(){
    this.competitionService.getAllCompetition().subscribe(res=>{
      this.comptetition = res;
      console.log(this.comptetition);
    });
  }
  getAllWeeks(){
    this.weekServices.getAllWeek().subscribe(res=>{
      this.week = res;
      console.log(this.week);
    });
  }
  getAllGames(){
    this.gameServices.getAllGame().subscribe(res=>{
      this.games = res;
      console.log(this.games);
    });
  }
  selectCompetitionLog(text:any){
    this.phase=[];
    this.week=[];
    this.games=[];
    this.phaseService.getPhaseByCompetitionId(text.value).subscribe(res=>{
      this.phase = res;
      console.log(this.phase);
    });
  }
  getClassementByPhase_Id(phaseId:string){
    this.classement = [];
    this.classementService.getClassementByPhaseId(phaseId).subscribe(res=>{
      this.classement = res;
    });
  }
  selectPhaseLog(event:any){
    this.week=[];
    this.games=[];
    this.getClassementByPhase_Id(event.value);
    this.weekServices.getWeekByPhaseId(event.value).subscribe(res=>{
    this.week = res;
    });
  }
  selectWeekLog(event:any){
    this.games=[];
    this.gameServices.getGameByWeekId(event.value).subscribe(res=>{
      this.games=res;
      });
  }
  initClassementByPhaseId(){
    this.phaseService.getPhaseByCompetitionId("659ab68df8b60d07205188ce").subscribe(res=>{
      this.phase = res;
     this.getClassementByPhase_Id(this.phase[this.phase.length -1].id);
    });
  }
}