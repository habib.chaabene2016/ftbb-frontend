import { Component, OnInit } from '@angular/core';
declare const jqueryMigrate: any;
declare const jquery:any;
declare const bootstrapBundleMin:any;
declare const core:any;
declare const initJs:any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  ngOnInit(): void {
    jquery();
    jqueryMigrate();
    bootstrapBundleMin();
    core();
    initJs();
  }
  title = 'ftbb';
}
