import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'
});

const studuim_url = "https://ftbb.onrender.com/api/studuim";

@Injectable({
  providedIn: 'root'
})
export class StuduimService {

  constructor(private httpClient:HttpClient) { }

  getAllStuduim(): Observable<any>{
    return this.httpClient.get<any>(
      studuim_url,
      {headers});
  }
}
