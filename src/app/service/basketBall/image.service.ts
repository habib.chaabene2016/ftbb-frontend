import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'
});
const baseUrl = "https://ftbb.onrender.com/api/storage"
@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private httpClient:HttpClient) { }

  public readImage(imageRequest: any): Observable<ArrayBuffer> {
    return this.httpClient.get(
      baseUrl+'/getimage/'+imageRequest,
      { responseType: 'arraybuffer' }
    ) as Observable<ArrayBuffer>;
  }
}
