import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'
});
const game_url = "https://ftbb.onrender.com/api/game";
@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private httpClient:HttpClient) { }

  getAllGame(): Observable<any>{
    return this.httpClient.get<any>(
      game_url,
      {headers});
  }
  addCompetition(){}
  getGameByWeekId(id:any): Observable<any>{
    return this.httpClient.get<any>(
      game_url+'/byWeek/'+id,
      {headers});
  }
}