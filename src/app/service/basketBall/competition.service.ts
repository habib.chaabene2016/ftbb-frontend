import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'
});
const competition_url = "https://ftbb.onrender.com/api/competition";
@Injectable({
  providedIn: 'root'
})
export class CompetitionService {

  constructor(private httpClient:HttpClient) { }

  getAllCompetition(): Observable<any>{
    return this.httpClient.get<any>(
      competition_url+'/allCompetition',
      {headers});
  }
  addCompetition(){
      //
  }
}
