import { TestBed } from '@angular/core/testing';

import { StuduimService } from './studuim.service';

describe('StuduimService', () => {
  let service: StuduimService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StuduimService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
