import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'
});

const week_url = "https://ftbb.onrender.com/api/week";
@Injectable({
  providedIn: 'root'
})
export class WeekService {

  constructor(private httpClient:HttpClient) { }
  getAllWeek(): Observable<any>{
    return this.httpClient.get<any>(
      week_url,
      {headers});
  }
  getWeekByPhaseId(id:any): Observable<any>{
    return this.httpClient.get<any>(
      week_url+'/byPhaseId/'+id,
      {headers});
  }
}
