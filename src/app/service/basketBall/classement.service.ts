import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'
});
const classement_url = "https://ftbb.onrender.com/api/classement";
@Injectable({
  providedIn: 'root'
})
export class ClassementService {

  constructor(private httpClient:HttpClient) { }
  getClassementByPhaseId(id:any): Observable<any>{
    return this.httpClient.get<any>(
      classement_url+'/'+id,
      {headers});
  }

}
