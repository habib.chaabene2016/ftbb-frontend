import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'
});
const phase_url = "https://ftbb.onrender.com/api/phase";
@Injectable({
  providedIn: 'root'
})
export class PhaseService {

  constructor(private httpClient:HttpClient) { }
  getAllPhase(): Observable<any>{
    return this.httpClient.get<any>(
      phase_url,
      {headers});
  }
  getPhaseByCompetitionId(id:any): Observable<any>{
    return this.httpClient.get<any>(
      phase_url+'/CompetitionId/'+id,
      {headers});
  }
}
