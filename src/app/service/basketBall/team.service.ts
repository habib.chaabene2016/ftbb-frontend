import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'
});

const team_url = "https://ftbb.onrender.com/api/team";

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private httpClient:HttpClient) { }

  getAllTeam(): Observable<any>{
    return this.httpClient.get<any>(
      team_url,
      {headers});
  }
}
